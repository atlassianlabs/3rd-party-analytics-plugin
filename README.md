# README #

This is an Atlassian Plugin for injecting the Google Analytics script into each page.

### What is this repository for? ###

This plugin will allow you to measure Application usage via Google Analytics.
It requires you to bring your own Google Analytics Key which can be configured by a site admin.
NB: This means, page titles and possibly other data is sent to Google Analytics from each user's browser.

![Screenshot 2015-04-17 13.52.15.png](https://bitbucket.org/repo/e9bqRp/images/3697165514-Screenshot%202015-04-17%2013.52.15.png)

### To build this plugin: ###

```
#!java
$ git clone ...
$ mvn stash:debug
```
You can then login using admin/admin .


