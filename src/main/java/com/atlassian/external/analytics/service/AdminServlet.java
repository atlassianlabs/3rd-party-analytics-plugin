package com.atlassian.external.analytics.service;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


/**
 */
public class AdminServlet extends HttpServlet {

    private final SoyTemplateRenderer templateRenderer;
    private final ConfigurationManager configurationManager;
    private final UserManager userManager;

    final static String PARAM_GA_ID = "gaID";
    final static String PARAM_NR_LICENSE_KEY = "nrLicenseKey";
    final static String PARAM_NR_APPLICATION_ID = "nrApplicationID";
    private static final String TEMPLATE_RESOURCE_MODULE_KEY = "com.atlassian.plugins.external-analytics.external-analytics-plugin:external-analytics-soy-template";
    private static final String TEMPLATE_NAME = "com.atlassian.plugins.external.analytics.config";

    private static final Logger LOG = LoggerFactory.getLogger(AdminServlet.class);

    public AdminServlet(SoyTemplateRenderer templateRenderer, ConfigurationManager configurationManager, UserManager userManager) {
        this.templateRenderer = templateRenderer;
        this.configurationManager = configurationManager;
        this.userManager = userManager;
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        UserProfile user = userManager.getRemoteUser(req);
        if (user == null || !userManager.isAdmin(user.getUserKey())) {
            resp.setStatus(401);
            return;
        }


        setIfNotNull(req, PARAM_GA_ID, configurationManager::putTrackingID);
        setIfNotNull(req, PARAM_NR_APPLICATION_ID, configurationManager::putNewrelicApplicationID);
        setIfNotNull(req, PARAM_NR_LICENSE_KEY, configurationManager::putNewRelicLicenseKey);

        final Map<String, Object> context = new HashMap<>();

        context.put(PARAM_GA_ID, configurationManager.getTrackingID());
        context.put(PARAM_NR_LICENSE_KEY, configurationManager.getNewRelicLicenseKey());
        context.put(PARAM_NR_APPLICATION_ID, configurationManager.getNewrelicApplicationID());

        resp.setContentType("text/html;charset=utf-8");
        try {
            templateRenderer.render(resp.getWriter(), TEMPLATE_RESOURCE_MODULE_KEY, TEMPLATE_NAME, context);
        } catch (SoyException e) {
            LOG.warn(e.getMessage(), e);
        }

    }

    private void setIfNotNull(HttpServletRequest req,
                              String name,
                              Function<String, Object> set) {
        final String key = req.getParameter(name);
        if (key != null) {
            set.apply(key);
        }
    }

}
