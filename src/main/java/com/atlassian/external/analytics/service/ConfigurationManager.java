package com.atlassian.external.analytics.service;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

public class ConfigurationManager {

    private static final String PLUGIN_STORAGE_KEY = "google.analytics.storage";
    private static final String GA_TRACKING_ID = "ga-tracking-id";
    private static final String NEWRELIC_LICENSE_KEY = "nr-license-key";
    private static final String NEWRELIC_APPLICATION_ID = "nr-application-id";

    private final PluginSettings settings;

    public ConfigurationManager(PluginSettingsFactory pluginSettingsFactory) {
        settings = pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY);
    }

    public String getTrackingID() {
        return getValue(GA_TRACKING_ID);
    }

    public Object putTrackingID(String trackingID) {
        return settings.put(GA_TRACKING_ID, trackingID);
    }

    public String getNewRelicLicenseKey() {
        return getValue(NEWRELIC_LICENSE_KEY);
    }

    public Object putNewRelicLicenseKey(String value) {
        return settings.put(NEWRELIC_LICENSE_KEY, value);
    }

    public String getNewrelicApplicationID() {
        return getValue(NEWRELIC_APPLICATION_ID);
    }

    public Object putNewrelicApplicationID(String value) {
        return settings.put(NEWRELIC_APPLICATION_ID, value);
    }

    private String getValue(String storageKey) {
        Object storedValue = settings.get(storageKey);
        return storedValue == null ? "" : storedValue.toString();
    }


}