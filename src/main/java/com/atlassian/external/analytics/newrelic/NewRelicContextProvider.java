package com.atlassian.external.analytics.newrelic;

import com.atlassian.external.analytics.service.ConfigurationManager;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class NewRelicContextProvider implements ContextProvider {

    private final ConfigurationManager configurationManager;
    private final ApplicationProperties propertiesService;

    public NewRelicContextProvider(ConfigurationManager configurationManager,
                                  ApplicationProperties propertiesService) {
        this.configurationManager = configurationManager;
        this.propertiesService = propertiesService;
    }

    @Override
    public void init(Map<String, String> map) throws PluginParseException { }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> map) {
        Map<String, Object> context = new HashMap<>(2);
        context.put("applicationID", configurationManager.getNewrelicApplicationID());
        context.put("licenseKey", configurationManager.getNewRelicLicenseKey());
        return context;
    }
}
