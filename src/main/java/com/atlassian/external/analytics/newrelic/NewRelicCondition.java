package com.atlassian.external.analytics.newrelic;

import com.atlassian.external.analytics.service.ConfigurationManager;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

public class NewRelicCondition implements Condition {

    private final ConfigurationManager configurationManager;

    public NewRelicCondition(ConfigurationManager configurationManager) {
        this.configurationManager = configurationManager;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException { }

    @Override
    public boolean shouldDisplay(Map<String, Object> context) {
        return !"".equals(configurationManager.getNewrelicApplicationID()) &&
                !"".equals(configurationManager.getNewRelicLicenseKey());
    }
}
