package com.atlassian.external.analytics.google;

import com.atlassian.external.analytics.service.ConfigurationManager;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class SnippetContextProvider implements ContextProvider {

    private final ConfigurationManager configurationManager;
    private final ApplicationProperties propertiesService;

    public SnippetContextProvider(ConfigurationManager configurationManager,
                                  ApplicationProperties propertiesService) {
        this.configurationManager = configurationManager;
        this.propertiesService = propertiesService;
    }

    @Override
    public void init(Map<String, String> map) throws PluginParseException { }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> map) {
        Map<String, Object> context = new HashMap<>(2);
        context.put("trackingID", configurationManager.getTrackingID());
        String baseUrl = propertiesService.getBaseUrl(UrlMode.ABSOLUTE);
        URL url;
        try {
            url = new URL(baseUrl);
            context.put("hostname", url.getHost());
        } catch (MalformedURLException e) {
            context.put("hostname", "auto");
        }

        return context;
    }
}
