package com.atlassian.external.analytics.google;

import com.atlassian.external.analytics.service.ConfigurationManager;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

public class SnippetCondition implements Condition {

    private final ConfigurationManager configurationManager;

    public SnippetCondition(ConfigurationManager configurationManager) {
        this.configurationManager = configurationManager;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException { }

    @Override
    public boolean shouldDisplay(Map<String, Object> context) {
        return !"".equals(configurationManager.getTrackingID());
    }
}
